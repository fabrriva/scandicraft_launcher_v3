### Template docs

https://electron-react-boilerplate.js.org/docs/installation

### Require

node, git, yarn

### Installation

yarn

### Dev

yarn dev

### Prod

OPEN_ANALYZER=true yarn build
yarn package --win
yarn package --linux
yarn package --mac //depuis un mac

# debug prod

yarn cross-env DEBUG_PROD=true yarn build
yarn cross-env DEBUG_PROD=true yarn start

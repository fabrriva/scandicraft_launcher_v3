package net.scandicraft;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import fr.theshark34.supdate.SUpdate;
import fr.theshark34.supdate.application.Application;
import fr.theshark34.supdate.application.event.ApplicationEvent;
import fr.theshark34.supdate.application.event.FileCheckingEvent;
import fr.theshark34.supdate.application.event.fileaction.FileActionEvent;
import fr.theshark34.supdate.exception.BadServerResponseException;
import fr.theshark34.supdate.exception.FileNoPermissionException;

public class FileDeleter extends Application
{
  private List<String> ignoreList = new ArrayList();

  public String getName()
  {
    return "FileDeleter";
  }

  public boolean isServerRequired()
  {
    return true;
  }

  public void onInit(ApplicationEvent event)
  {
  }

  public void onStart(ApplicationEvent event)
  {
	  ignoreList.add("optionsof.txt");
	  ignoreList.add("options.txt");
	  ignoreList.add("resourcepacks/");
	  ignoreList.add("lanceur/");
	  ignoreList.add("config.json");
  }

  public boolean onFileChecking(FileCheckingEvent event)
  {
    if (this.ignoreList == null)
    {
      return event.getCheckResult();
    }

    this.ignoreList.add(event.getCheckedFilePath());

    return event.getCheckResult();
  }

  public void onFileAction(FileActionEvent event)
  {
  }

  public void onUpdateEnd(ApplicationEvent event)
  {
    SUpdate.logger.info("[FileDeleter] Deleting the unknown files", new Object[0]);
    
    for (File file : listFiles(event.getSUpdate().getOutputFolder()))
    {
      if (!isOnIgnoreList(event.getSUpdate(), file))
        try
        {
          SUpdate.logger.info("[FileDeleter] Deleting file '%s'.", new Object[] { file.getAbsolutePath() });
          event.getSUpdate().getFileManager().delete(file);
        } catch (FileNoPermissionException e) {
          SUpdate.logger.warning("[FileDeleter] The file '" + file.getAbsolutePath() + "' wasn't deleted, error :", e);
        }
    }
  }

  public static ArrayList<File> listFiles(File folder)
  {
    File[] files = folder.listFiles();

    ArrayList list = new ArrayList();

    if (files == null) {
      return list;
    }
    for (File f : files) {
      if (f.isDirectory())
        list.addAll(listFiles(f));
      else
        list.add(f);
    }
    return list;
  }

  public boolean isOnIgnoreList(SUpdate sUpdate, File file)
  {
    for (String ignoredFilePath : this.ignoreList)
    {
      File ignoredFile = new File(sUpdate.getOutputFolder(), ignoredFilePath);

      if ((ignoredFile.getAbsolutePath().equals(file.getAbsolutePath())) || (file.getAbsolutePath().contains(ignoredFile.getAbsolutePath())))
      {
        return true;
      }

    }

    return false;
  }
}
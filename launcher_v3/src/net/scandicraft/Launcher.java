package net.scandicraft;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.external.ExternalLaunchProfile;
import fr.theshark34.openlauncherlib.external.ExternalLauncher;
import fr.theshark34.openlauncherlib.minecraft.AuthInfos;
import fr.theshark34.openlauncherlib.minecraft.GameFolder;
import fr.theshark34.openlauncherlib.minecraft.GameInfos;
import fr.theshark34.openlauncherlib.minecraft.GameTweak;
import fr.theshark34.openlauncherlib.minecraft.GameType;
import fr.theshark34.openlauncherlib.minecraft.GameVersion;
import fr.theshark34.openlauncherlib.minecraft.MinecraftLauncher;
import fr.theshark34.supdate.BarAPI;
import fr.theshark34.supdate.SUpdate;
import fr.theshark34.supdate.application.integrated.FileDeleter;

public class Launcher
{

	public static final GameVersion SC_VERSION = new GameVersion("1.7.10", GameType.V1_7_10);
	public static final GameInfos SC_INFOS = new GameInfos("ScandiCraft", SC_VERSION, new GameTweak[] {});
	public static final File SC_DIR = SC_INFOS.getGameDir();
	private static Thread updateThread;

	public static void update() throws Exception
	{
		final SUpdate su = new SUpdate("http://download.scandicraft.fr/lanceur", SC_DIR);
		su.addApplication(new net.scandicraft.FileDeleter());

		updateThread = new Thread()
		{
			@Override
			public void run()
			{
				int percentage;
				do
				{
					int val = (int) (BarAPI.getNumberOfTotalDownloadedBytes() / 1000);
					int max = (int) (BarAPI.getNumberOfTotalBytesToDownload() / 1000);

					LauncherFrame.getInstance().getLauncherPanel().getBar().setMaximum(max);
					LauncherFrame.getInstance().getLauncherPanel().getBar().setValue(val);

					percentage = (int) ((double) val / (double) max * (double) 100);
					if (percentage > 0)
						LauncherFrame.getInstance().getLauncherPanel().setInfoText("Téléchargement des fichiers en cours.. " + BarAPI.getNumberOfDownloadedFiles() + "/" + BarAPI.getNumberOfFileToDownload() + "   -   " + percentage + "%");
					else
						LauncherFrame.getInstance().getLauncherPanel().setInfoText("Analyse de votre .ScandiCraft en cours");
				}
				while (!this.isInterrupted());

			}
		};
		updateThread.start();

		su.start();
	}

	public static void launch(AuthInfos authInfos) throws InterruptedException, IOException
	{
		
		// updateThread.interrupt();

		try{
			ExternalLaunchProfile profile = MinecraftLauncher.createExternalProfile(SC_INFOS, GameFolder.BASIC, authInfos);
			//Xms = démarrage ; Xmm = max
			List<String> vmArgs = Arrays.asList("-Xms1024M", "-Xmx2048M");
			profile.setVmArgs(vmArgs);
			ExternalLauncher launcher = new ExternalLauncher(profile);
			launcher.launch();
            
			//JOptionPane.showMessageDialog(LauncherFrame.getInstance(), "debug ", "Launch demo...", JOptionPane.INFORMATION_MESSAGE);

			System.exit(0);
		}
		catch (LaunchException e)
		{
            JOptionPane.showMessageDialog(LauncherFrame.getInstance(), "debug ", "Erreur", JOptionPane.ERROR_MESSAGE);

			e.printStackTrace();
		}
	}
}
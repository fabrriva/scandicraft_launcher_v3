package net.scandicraft;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public class LauncherConfig {
	public String sChemin;
	   
	public LauncherConfig(String sPath){
		this.sChemin = sPath;
	}
	
	public void writeFile(JSONObject objJSON)
	{
   		try {
   			File f = new File(sChemin);
   			System.out.println(sChemin);
   			if(!f.exists())
   			{
   				f.getParentFile().mkdirs();
   				f.createNewFile();
   			}
   			
   			FileWriter file = new FileWriter(sChemin);
   			file.write(objJSON.toString());
   			file.flush();
   			file.close();

   		} catch (IOException e) {
   			e.printStackTrace();
   		}
	}
	
	public void writeJSON(String sUsername, String sPasswordMD5, Boolean bShaders, String sRam, Boolean bArmor, Boolean bPotion) 
	{
		JSONObject json = new JSONObject();
   	 
   	 	try {
			json.put("username", sUsername);
			json.put("password", sPasswordMD5);
			json.put("shaders", bShaders);
			json.put("ram", sRam);
			json.put("armureHUD", bArmor);
			json.put("potionHUD", bPotion);
   	 	} catch (JSONException e) {
			e.printStackTrace();
		}
   	 	writeFile(json);
   	}
	
	public Boolean writeJSON(String sUsername) {
		Boolean bWrite = true;
		
		JSONObject json = new JSONObject();
   	 	try {
			json.put("username", sUsername);
   	 	} catch (JSONException e) {
			e.printStackTrace();
			bWrite = false;
		}
   	 	writeFile(json);
   	 	
   	 	return bWrite;
	}
   
   public String getJSON()
   {
   	String jsonData = "";
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new FileReader(sChemin));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return jsonData;
   }
   
   public String getUser() throws JSONException {
   	String sUser = "";
   	
   	JSONObject obj = new JSONObject(getJSON());
		try {
			sUser = obj.getString("username");
		} catch (JSONException e) {
			e.printStackTrace();
		}
   	
   	return sUser;
   }
   
   public String getPassword() throws Exception {
   	String sPassword = "";
   	
   	JSONObject obj = new JSONObject(getJSON());
		try {
			sPassword = obj.getString("password");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
   	return sPassword;
   }
   
   public Boolean getShaders() throws Exception {
	   	Boolean bShaders = null;
	   	
	   	JSONObject obj = new JSONObject(getJSON());
			try {
				bShaders = obj.getBoolean("shaders");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
	   	return bShaders;
	}
   
   public Boolean getArmor() throws Exception {
	   	Boolean bArmor = null;
	   	
	   	JSONObject obj = new JSONObject(getJSON());
			try {
				bArmor = obj.getBoolean("armureHUD");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
	   	return bArmor;
	}
   
   public Boolean getPotion() throws Exception {
	   	Boolean bPotion = null;
	   	
	   	JSONObject obj = new JSONObject(getJSON());
			try {
				bPotion = obj.getBoolean("potionHUD");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
	   	return bPotion;
	}
   
   
   public String getRam() throws Exception {
	   	String sRam = "";
	   	
	   	JSONObject obj = new JSONObject(getJSON());
			try {
				sRam = obj.getString("ram");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
	   	return sRam;
   }
   
}

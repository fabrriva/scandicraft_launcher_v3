package net.scandicraft;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.event.SwingerEvent;
import fr.theshark34.swinger.event.SwingerEventListener;
import fr.theshark34.swinger.textured.STexturedButton;
import fr.theshark34.swinger.util.WindowMover;

@SuppressWarnings("serial")
public class LauncherOptions extends JFrame implements SwingerEventListener, ActionListener {
	private STexturedButton btnQuit = new STexturedButton(Swinger.getResource("quit.png"));
	private JButton btnSave = new JButton();
	
	public LauncherOptions()
	{
		this.setTitle("[ScandiCraft] Options");
		this.setSize(380, 400);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setIconImage(Swinger.getResource("icon.png"));
		WindowMover mover = new WindowMover(this);
		this.addMouseListener(mover);
		this.addMouseMotionListener(mover);
		this.setUndecorated(true);
		this.setContentPane(new Panneau());
		this.setVisible(true);
		this.setLayout(null);
		
		LauncherFrame.getInstance().setEnabled(false);
			
		btnQuit.setBounds(339, 3);
		btnQuit.addEventListener(this);
		add(btnQuit);
		
		btnSave.setBounds(102, 351, 172, 28);
		btnSave.setOpaque(false);
		btnSave.setContentAreaFilled(false);
		btnSave.setFocusPainted(false);
		btnSave.addActionListener(this);
		add(btnSave);
	}
	
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource() == btnSave)
    	{
        	//R�cup�ration des infos shaders et ram
        	String sInfos = "";
    	    sInfos = "Les informations suivantes seront enregistr�es:\n";
        	String sPath = Launcher.SC_DIR.toString();
        	sInfos += "Chemin: " + sPath.toString();
    	    //Confirmation des infos
        	JOptionPane.showMessageDialog(LauncherFrame.getInstance(), sInfos, "ScandiCraft - Informations", JOptionPane.INFORMATION_MESSAGE);
        	//Fermeture de la fenetre
        	this.dispose();
			//On mets r�active la f�netre principal et on la mets au dessus
			LauncherFrame.getInstance().setEnabled(true);
			LauncherFrame.getInstance().setAlwaysOnTop(true);
			LauncherFrame.getInstance().setAlwaysOnTop(false);
    	}
    }
	
	public void onEvent(SwingerEvent e) {
		if(e.getSource() == btnQuit)
		{
			//Ferme la fenetre
			this.dispose();
			//On mets r�active la f�netre principal et on la mets au dessus
			LauncherFrame.getInstance().setEnabled(true);
			LauncherFrame.getInstance().setAlwaysOnTop(true);
			LauncherFrame.getInstance().setAlwaysOnTop(false);
		}		
	}
	
	private class Panneau extends JPanel {
		private Image background = Swinger.getResource("pOptions.png");

		public void paintComponent(Graphics g) {
			g.drawImage(this.background, 0, 0, getWidth(), getHeight(), this);
		}
	}
	
	public static void main(String[] args) {
		Swinger.setSystemLookNFeel();
		Swinger.setResourcePath("/net/scandicraft/ressources");
	}
	
}

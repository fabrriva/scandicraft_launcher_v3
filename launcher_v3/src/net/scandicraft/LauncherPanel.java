package net.scandicraft;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.UUID;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.google.gson.JsonSyntaxException;

import fr.theshark34.openauth.AuthPoints;
import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openauth.Authenticator;
import fr.theshark34.openauth.model.AuthAgent;
import fr.theshark34.openauth.model.response.AuthResponse;
import fr.theshark34.openlauncherlib.minecraft.AuthInfos;
import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.colored.SColoredBar;
import fr.theshark34.swinger.event.SwingerEvent;
import fr.theshark34.swinger.event.SwingerEventListener;
import fr.theshark34.swinger.textured.STexturedButton;

@SuppressWarnings("serial")
public class LauncherPanel extends JPanel implements SwingerEventListener
{

	private Image background = Swinger.getResource("background.png");

	private JTextField txtUser = new JTextField();

	private STexturedButton btnPlay = new STexturedButton(Swinger.getResource("play.png"));
	private STexturedButton btnQuit = new STexturedButton(Swinger.getResource("quit.png"));
	private STexturedButton btnHide = new STexturedButton(Swinger.getResource("hide.png"));
	public STexturedButton btnOptions = new STexturedButton(Swinger.getResource("options.png"));

	private SColoredBar bar = new SColoredBar(new Color(255, 255, 255, 15), new Color(16, 216, 56, 80));
	private JLabel lblInfos = new JLabel("Cliquez sur Jouer !", SwingConstants.CENTER);

	private ArrayList<String> banList = new ArrayList<String>();
	private int minCar = 3;
	private String sPath = Launcher.SC_DIR.toString() + "\\config.json";
	private LauncherConfig LauncherConfig = new LauncherConfig(sPath);
	private String sPseudo;
	private String sMotdepasse;
	private static Boolean bShaders = false;
	private static Boolean bArmor = true;
	private static Boolean bPotion = true;
	private static String sRam = "1GO";
	private Boolean bConfigRecup = false;
	private static int iRamSelect = 512;

	public LauncherPanel()
	{
		setLayout(null);

		txtUser = new JTextField();
		txtUser.setHorizontalAlignment(JTextField.CENTER);
		txtUser.setForeground(Color.WHITE);
		txtUser.setFont(txtUser.getFont().deriveFont(20F));
		txtUser.setCaretColor(Color.WHITE);
		txtUser.setOpaque(false);
		txtUser.setBorder(null);
		txtUser.setBounds(98, 543, 262, 39);
		add(txtUser);

		btnPlay.setBounds(82, 622);
		btnPlay.addEventListener(this);
		add(btnPlay);

		btnQuit.setBounds(404, 8);
		btnQuit.addEventListener(this);
		add(btnQuit);

		btnHide.setBounds(360, 8);
		btnHide.addEventListener(this);
		add(btnHide);

		btnOptions.setBounds(4, 4);
		btnOptions.addEventListener(this);
		add(btnOptions);

		bar.setStringPainted(true);
		bar.setBounds(86, 456, 284, 22);
		add(bar);

		lblInfos.setForeground(Color.WHITE);
		lblInfos.setBounds(86, 456, 284, 22);
		add(lblInfos);

		banList.add(" ");
		banList.add("!");
		banList.add("#");
		banList.add("$");
		banList.add("&");
		banList.add("'");
		banList.add("(");
		banList.add(")");
		banList.add("*");
		banList.add("+");
		banList.add(",");
		banList.add("-");
		banList.add(".");
		banList.add("/");
		banList.add("@");
		banList.add("?");
		banList.add(">");
		banList.add("=");
		banList.add("<");
		banList.add("[");
		banList.add("]");
		banList.add("\"");
		banList.add("*");
		banList.add("\\");

		// Si le fichier config.json est existant
		if (new File(sPath).exists())
		{
			System.out.println("[ScandiCraft - Config] R�cup�ration de config.json");
			// On r�cup�re sa config
			try
			{
				bConfigRecup = true;
				ChercherConfig();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			// Sinon on ne fait rien
			System.out.println("[ScandiCraft - Config] config.json inexistant");
		}
	}

	public void setInfoText(String text)
	{
		lblInfos.setText(text);
	}

	@Override
	public void onEvent(SwingerEvent e)
	{
		if (e.getSource() == btnQuit)
		{
			System.exit(0);
		}
		else if (e.getSource() == btnHide)
		{
			LauncherFrame.getInstance().setState(JFrame.ICONIFIED);
		}
		else if (e.getSource() == btnOptions)
		{
			LauncherOptions op = new LauncherOptions();
		}
		else if (e.getSource() == btnPlay)
		{
			//On save le pseudo
			Boolean bWrite = LauncherConfig.writeJSON(txtUser.getText());
			//System.out.println(bWrite);
			
			if (txtUser.getText().length() > minCar) {
				Jouer();
			} else {
				JOptionPane.showMessageDialog(LauncherFrame.getInstance(), "Pseudo trop court", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
    private void Jouer() {
        Thread t = new Thread() {
            @Override
            public void run() {
            	txtUser.setEnabled(false);
                btnPlay.setEnabled(false);

                /*
                try {
                    Launcher.update();
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(LauncherFrame.getInstance(), "Impossible de mettre � jour le jeu ! : " + e, "Erreur", JOptionPane.ERROR_MESSAGE);

                    txtUser.setEnabled(true);
                    btnPlay.setEnabled(true);

                    return;
                }
                */

                AuthInfos authInfos = new AuthInfos(txtUser.getText(), "null", "null");

                try {
                    Launcher.launch(authInfos);
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(LauncherFrame.getInstance(), "Impossible de lancer le jeu ! : " + e, "Erreur", JOptionPane.ERROR_MESSAGE);

                    txtUser.setEnabled(true);
                    btnPlay.setEnabled(true);

                    return;
                } catch (InterruptedException e) {
                } finally {
                    System.exit(0);
                }
            }
        };
        t.start();
    }

	public static void ModifieOptions(Boolean bConfShaders, String sConfRam, Boolean bConfArmor, Boolean bConfPotion)
	{
		bShaders = bConfShaders;
		sRam = sConfRam;
		bArmor = bConfArmor;
		bPotion = bConfPotion;
	}

	public static Boolean recupConfShaders()
	{
		return bShaders;
	}
	
	public static Boolean recpConfArmor()
	{
		return bArmor;
	}
	
	public static Boolean recpConfPotion()
	{
		return bPotion;
	}

	public static String recupConfRam()
	{
		return sRam;
	}

	public static int getRamSelect()
	{
		if (sRam.equals("1 GO"))
		{
			iRamSelect = 1;
		}
		else if (sRam.equals("2 GO"))
		{
			iRamSelect = 2;
		}
		else if (sRam.equals("3 GO"))
		{
			iRamSelect = 3;
		}
		else if (sRam.equals("4 GO"))
		{
			iRamSelect = 4;
		}
		return iRamSelect;
	}

	public SColoredBar getBar()
	{
		return this.bar;
	}

	private void ChercherConfig() throws Exception
	{
		// On r�cup�re le pseudo
		sPseudo = LauncherConfig.getUser();
		txtUser.setText(sPseudo);
		/*
		// On r�cup�re le mot de passe md5
		sMotdepasse = LauncherConfig.getPassword();
		// On r�cup�re si shaders ou non
		bShaders = LauncherConfig.getShaders();
		// On r�cup�re la ram
		sRam = LauncherConfig.getRam();
		// On affecte la valeur dans les textbox
		txtUser.setText(sPseudo);
		//On r�cup�re HUD Armure
		bArmor = LauncherConfig.getArmor();
		//On r�cup�re HUD potion
		bPotion = LauncherConfig.getPotion();
		*/
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this);
	}

}
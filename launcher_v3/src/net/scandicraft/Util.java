package net.scandicraft;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;

public class Util
{
	public static byte[] clientUniqueID()
    {
        byte[] mac = null;
        try
        {
            Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
            while (nis.hasMoreElements())
            {
                NetworkInterface ni = nis.nextElement();
                if (ni.isUp() && ni.getHardwareAddress() != null && !ni.isLoopback() && !ni.isVirtual() && !ni.getDisplayName().toLowerCase().contains("tunnel"))
                {
                    mac = ni.getHardwareAddress();
                    return mac;
                }
            }
        } catch (SocketException ex)
        {
        }
        byte[] b = new byte[20];
        new Random().nextBytes(b );
        return b;
    }
}
